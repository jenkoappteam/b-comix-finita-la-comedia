package jenko.b_comix;

import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;
import android.support.v7.widget.Toolbar;
import com.facebook.AccessToken;
import com.facebook.login.LoginManager;
import com.google.firebase.auth.FirebaseAuth;

public class Options extends AppCompatActivity {



    private FirebaseAuth mAuth;
    private LoginManager fManager;

    private Button logOut;
    private Button about;
    private Button restartDefault;
    private Button editProf;
    private Button sound;

    private Toolbar bar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_options);
        mAuth = FirebaseAuth.getInstance();

        bar = (Toolbar) findViewById(R.id.toolbar2);
        setSupportActionBar(bar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        bar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        logOut = (Button) findViewById(R.id.log_out);
        logOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseAuth.getInstance().signOut();
                LoginManager.getInstance().logOut();
                Toast.makeText(Options.this, "Signed Out Successfuly", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(Options.this, MainActivity.class));
                finish();
            }
        });


        about = (Button) findViewById(R.id.about_us);
        about.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Options.this, AboutUs.class));
            }
        });


        restartDefault = (Button) findViewById(R.id.restart_default);
        editProf = (Button) findViewById(R.id.edit_profile);
        editProf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Options.this, EditProfile.class));
            }
        });
        sound = (Button) findViewById(R.id.sound_notification);

    }


}

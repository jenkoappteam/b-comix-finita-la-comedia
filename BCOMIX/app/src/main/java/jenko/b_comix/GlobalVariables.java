package jenko.b_comix;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class GlobalVariables {

    public static String mName;
    public static String mDstUserID;
    public static String mEmail;
    public static String mScreen;
    public static int mChlidCount;
    public static String mMyNickname;
    public static String mMyGendre;
    public static String mAge;
    public static String mFigure;
    public static Set<String> mSelectedUsers;
    public static String mChatType;
    public static String mGroupID;
    GlobalVariables(){}
    public void setName(String name){mName = name;}
    public void setEmail(String email){mEmail = email;}
    public void setScreen(String screen){mScreen = screen;}
    public void setChildCount(int count){mChlidCount = count;}
    public void setMyNickname(String name){mMyNickname = name;}
    public void setdstUserID(String id){mDstUserID = id;}
    public void setSelectedUsers(Set<String> selectedUsers){mSelectedUsers = selectedUsers;}
    public void setChatType(String type){mChatType = type;}
    public void setGroupID(String ID){mGroupID = ID;}
    public void setGendre(String gendre){mMyGendre = gendre;}
    public void setAge(String age){mAge = age;}
    public void setFigure(String figure){mFigure = figure;}

    public  String getName(){return mName ;}
    public  String getEmail(){return mEmail;}
    public  String getScreen(){return mScreen;}
    public  int getChildCount(){return mChlidCount;}
    public  String getMyNickname(){return mMyNickname;}
    public  String getDstUserID(){return mDstUserID ;}
    public Set<String> getSelectedUsers(){return mSelectedUsers;}
    public String getChatType(){return mChatType;}
    public String getGroupID(){return mGroupID;}
    public String getgendre(){return mMyGendre;}
    public String getAge(){return  mAge;}
    public String getFigure(){return mFigure;}







}

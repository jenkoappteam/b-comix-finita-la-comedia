package jenko.b_comix;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.common.util.NumberUtils;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UserInfoPickNameAge extends AppCompatActivity {

    private static final int NAME_TOO_LONG = 1;
    private static final int CONTAIN_SPECIAL_SIGNS = 2;
    private static final int VALID_NAME = 0;
    private static final int AGE_TOO_BIG = 1;
    private static final int AGE_IS_NOT_A_NUMBER = 2;
    private static final int VALID_AGE= 0;
    private boolean mAgeIsExist = false;
    private boolean mNameIsExist = false;
    private Button nextBtn;
    private GlobalVariables global;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_info_pick_name_age);
        global = new GlobalVariables();
        Typeface comicSensFont = Typeface.createFromAsset(getAssets(), "comic_sens.ttf");
        final EditText name_edit_text = (EditText) findViewById(R.id.nickNameEditText);
        final EditText age_edit_text = (EditText) findViewById(R.id.AgeEditText);
        nextBtn = (Button) findViewById(R.id.next_btn);

        name_edit_text.setTypeface(comicSensFont);
        age_edit_text.setTypeface(comicSensFont);

        name_edit_text.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            //changed
            @Override
            public void afterTextChanged(Editable s) {
                mNameIsExist = true;
                if(mNameIsExist && mAgeIsExist)
                {
                    nextBtn.setEnabled(true);
                }
                if(name_edit_text.getText().length() < 1)
                {
                    mNameIsExist = false;
                }
                //mExistsNicknamesDB.addValueEventListener(new ValueEventListener() {
                //    @Override
                //    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                //        if(dataSnapshot.exists()){
                //            if(dataSnapshot.getValue()==null){
                //               mCreateNamesSet = true;
                //           }
                //          else{
                //              mExiestsnames = (Map<String, Object>) dataSnapshot.getValue();
                //          }
                //       }
                //   }

                //  @Override
                //   public void onCancelled(@NonNull DatabaseError databaseError) {

                //    }
                // });
            }
        });

        age_edit_text.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                mAgeIsExist = true;
                if(mNameIsExist && mAgeIsExist)
                {
                    nextBtn.setEnabled(true);
                }
                if(age_edit_text.getText().length() < 1)
                {
                    mAgeIsExist = false;
                }
            }
        });

        nextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //if (HasGendre > 0 && (name_edit_text.getText() == null || age_edit_text.getText() == null)) {
                //    Toast.makeText(UserInfo.this, "All fields must be filled.", Toast.LENGTH_SHORT).show();
                if (mNameIsExist == false || mAgeIsExist == false || validNickname(name_edit_text.getText().toString()) != 0 || validAge(age_edit_text.getText().toString()) != 0) {
                    if (mNameIsExist == false || mAgeIsExist == false)
                    {
                        Toast.makeText(UserInfoPickNameAge.this, "All fields must be filled.", Toast.LENGTH_SHORT).show();
                    }
                    else if (validNickname(name_edit_text.getText().toString()) == NAME_TOO_LONG)
                    {
                        Toast.makeText(UserInfoPickNameAge.this, "Nickname is too long", Toast.LENGTH_SHORT).show();
                    }
                    else if (validNickname(name_edit_text.getText().toString()) == CONTAIN_SPECIAL_SIGNS)
                    {
                        Toast.makeText(UserInfoPickNameAge.this, "Nickname can not contain spacial signs", Toast.LENGTH_SHORT).show();
                    }
                    else if (validAge(age_edit_text.getText().toString()) == AGE_IS_NOT_A_NUMBER)
                    {
                        Toast.makeText(UserInfoPickNameAge.this, "Age is not a number", Toast.LENGTH_SHORT).show();
                    }
                    else if (validAge(age_edit_text.getText().toString()) == AGE_TOO_BIG)
                    {
                        Toast.makeText(UserInfoPickNameAge.this, "Are you " + age_edit_text.getText() + " years old?! congratulations", Toast.LENGTH_SHORT).show();
                    }
                }
                else {
                    global.setName(name_edit_text.getText().toString());
                    global.setAge(age_edit_text.getText().toString());
                    startActivity(new Intent(UserInfoPickNameAge.this, UserInfoSelectCharacter.class));

                }
            }
        });
    }

    private int validNickname(String nickname)
    {
        int upperLettersCount = getCountOfCapitalLetters(nickname);

        if(upperLettersCount == 0 && nickname.length() > 12)
        {
            return NAME_TOO_LONG;
        }

        if(upperLettersCount > 0 && upperLettersCount <= 6 && nickname.length() > 11)
        {
            return NAME_TOO_LONG;
        }

        if(upperLettersCount >= 7 && nickname.length() > 10)
        {
            return NAME_TOO_LONG;
        }

        if(!nickname.matches("[a-zA-Z0-9]*"))
        {
            return CONTAIN_SPECIAL_SIGNS;
        }
        return VALID_NAME;
    }

    private int getCountOfCapitalLetters(String str)
    {
        int i = 0; int count = 0;
        for(i = 0; i < str.length(); i++)
        {
            if (java.lang.Character.isUpperCase(str.charAt(i))) count++;
        }
        return count;
    }

    private int validAge(String age)
    {
        if(NumberUtils.isNumeric(age)) {
            if(Integer.parseInt(age) > 150) {
                return AGE_TOO_BIG;
            }
            return VALID_AGE;
        }
        return AGE_IS_NOT_A_NUMBER;
    }
}

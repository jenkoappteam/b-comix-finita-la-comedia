package jenko.b_comix;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

public class AboutUs extends AppCompatActivity {

    Toolbar toolbar;
    TextView text;
    String longText = "The company deals with the accompaniment of inventors and developers of physical products such as original stand for a smartphone or original application or a product that combines electronic capabilities and more ...\n" +
            "Targeted team of engineers and developers:\n" +
            "From the formulation of the concept to building a drawing / sketch\n" +
            "Ideas for product production and marketing lines.\n" +
            "And from there:\n" +
            "Building a prototype with a product portfolio optimized for production and page marketing estimates\n" +
            "And creates a good infrastructure to illustrate the product (prototype) and of course at a special and surprising price !!\n" +
            "The process can start with the desire to establish an original Egyptian line and we will open a line of products under your company name and in fact we will become a development department close to the company\n" +
            "You can come up with an idea for some product and we will make sure to make your idea a finished product\n" +
            "In the marketing department\n" +
            "We will offer you practical ideas to make the idea for money !!\n" +
            "Starting with Internet markets such as Amazon, Ebay and more\n" +
            "In addition, you can get product analyzes and market surveys,\n" +
            "Analysis of shortcomings versus advantages and according to findings new product design\n" +
            "Which is branded in your name. The company gives a wrapper for all your needs as beginner or advanced entrepreneurs.\n" +
            "In short, the best home for entrepreneurs!";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_us);

        text = (TextView) findViewById(R.id.brief);
        text.setText(longText);

        toolbar = (Toolbar) findViewById(R.id.toolbar3);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
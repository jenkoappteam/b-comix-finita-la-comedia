package jenko.b_comix;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import it.moondroid.coverflow.components.ui.containers.FeatureCoverFlow;

public class UserInfoSelectCharacter extends AppCompatActivity {

    private FeatureCoverFlow coverFlow;
    private UserSlideAdapter adapter;
    private ArrayList<Character> Characters;
    private String figure;
    private TextView mCharacterName;
    private Button nextBtn;
    boolean mCreateNamesSet;
    private FirebaseAuth mAuth;
    GlobalVariables global;
    private DatabaseReference mDatabase;

    private DatabaseReference mExistsNicknamesDB;
    Map<String,Object> mExiestsnames;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_info_select_character);
        mDatabase = FirebaseDatabase.getInstance().getReference();
        global = new GlobalVariables();
        mAuth = FirebaseAuth.getInstance();
        mExistsNicknamesDB = FirebaseDatabase.getInstance().getReference().child("nicknames");
        mExiestsnames = new HashMap<>();

        mExistsNicknamesDB.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    if(dataSnapshot.getValue()==null){
                        mCreateNamesSet = true;
                    }
                    else{
                        mExiestsnames = (Map<String, Object>) dataSnapshot.getValue();
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        nextBtn = (Button) findViewById(R.id.next_btn);
        coverFlow = (FeatureCoverFlow) findViewById(R.id.coverflow);
        mCharacterName = (TextView) findViewById(R.id.characterName);
        settingDummyData();
        adapter = new UserSlideAdapter(this, Characters);
        coverFlow.setAdapter(adapter);
        coverFlow.setOnScrollPositionListener(onScrollListener());

        nextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mCreateNamesSet){
                    Map<String,Object> usersNameMap = new HashMap<>();
                    usersNameMap.put(mAuth.getUid().toString(),global.getName());
                    mExistsNicknamesDB.setValue(usersNameMap);
                    User new_user = new User(global.getName(),global.getAge(),global.getgendre(),figure);
                    writeNewUser(mAuth.getUid(),new_user);
                    startActivity(new Intent(UserInfoSelectCharacter.this, LaunchActivity.class));
                    finish();
                }
                else{
                    mExiestsnames.put(mAuth.getUid().toString(),global.getName());
                    mExistsNicknamesDB.setValue(mExiestsnames);
                    User new_user = new User(global.getName(),global.getAge(),global.getgendre(),figure);
                    writeNewUser(mAuth.getUid(),new_user);
                    startActivity(new Intent(UserInfoSelectCharacter.this, LaunchActivity.class));
                    finish();
                }


                startActivity(new Intent(UserInfoSelectCharacter.this, LaunchActivity.class));
            }
        });
    }

    private FeatureCoverFlow.OnScrollPositionListener onScrollListener() {
        return new FeatureCoverFlow.OnScrollPositionListener() {
            @Override
            public void onScrolledToPosition(int position) {
                Log.v("MainActiivty", "position: " + position);
                pickFigure(position);
            }

            @Override
            public void onScrolling() {
                Log.i("MainActivity", "scrolling");
            }
        };
    }


    private void settingDummyData() {
        Characters = new ArrayList<>();
        Characters.add(new Character(R.drawable.grandpa_on_chair));
        Characters.add(new Character(R.drawable.grandpa_with_guitar));
        Characters.add(new Character(R.drawable.man_with_paresuit));
        Characters.add(new Character(R.drawable.man_with_radio));
        Characters.add(new Character(R.drawable.red_dress_women));
    }


    private void pickFigure(int curPos) {
        switch(curPos){
            case 0:
                figure = "grandpa_on_chair";
                mCharacterName.setText("Grandpa on chair");
                break;
            case 1:
                figure = "grandpa_with_guitar";
                mCharacterName.setText("Grandpa with guitar");
                break;
            case 2:
                figure = "man_with_paresuit";
                mCharacterName.setText("Man with a paresuit");
                break;
            case 3:
                figure = "man_with_radio";
                mCharacterName.setText("Man with a radio");
                break;
            case 4:
                figure = "red_dress_women";
                mCharacterName.setText("Red dress woman");
                break;
            default:
                figure = "red_dress_women";
                mCharacterName.setText("Red dress woman");
                break;
        }

    }

    private void writeNewUser(String userId,User user){
        DatabaseReference usernameDB = FirebaseDatabase.getInstance().getReference().child("nicknames");
        mExiestsnames.put(mAuth.getUid(),user.nickname);
        usernameDB.setValue(mExiestsnames);
        mDatabase.child("users").child(userId).setValue(user);
    }
}

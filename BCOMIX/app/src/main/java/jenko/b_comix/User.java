package jenko.b_comix;

import java.util.List;
import java.util.Map;

public class User{
    public String nickname;
    public String age;
    public String gendre;
    public String figure;

    public User(){

    }

    public User(String nickname,String age,String gendre,String figure){
        this.nickname = nickname;
        this.age = age;
        this.gendre = gendre;
        this.figure = figure;
    }

}

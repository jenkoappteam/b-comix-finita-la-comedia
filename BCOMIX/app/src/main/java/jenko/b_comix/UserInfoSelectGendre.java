package jenko.b_comix;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class UserInfoSelectGendre extends AppCompatActivity {

    private ImageView lightMale;
    private ImageView lightFemale;
    private ImageView male;
    private ImageView female;
    private String gendre;
    private boolean mIsGendreClicked = true;
    private Button nextBtn;
    private GlobalVariables global;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_info_select_gendre);
        global = new GlobalVariables();
        male = (ImageView) findViewById(R.id.MaleImageView);
        female = (ImageView) findViewById(R.id.FemaleImageView);
        lightMale = (ImageView) findViewById(R.id.MaleLightImageView);
        lightFemale = (ImageView) findViewById(R.id.FemaleLightImageView);
        //mLightGenderLayOut = (LinearLayout) findViewById(R.id.LightGenderLayOut);
        nextBtn = (Button) findViewById(R.id.next_btn);

        lightMale.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onClick(View v) {
                if(mIsGendreClicked == true)
                {
                    nextBtn.setEnabled(true);
                    mIsGendreClicked = false;
                }
                //HasGendre++;
                gendre = "male";
                if (male.getVisibility() == View.INVISIBLE) {
                    male.setVisibility(View.VISIBLE);
                    female.setVisibility(View.INVISIBLE);
                } else if (male.getVisibility() == View.VISIBLE) {
                    //male.setVisibility(View.INVISIBLE);
                }
                //name_edit_text.setTextColor(getResources().getColor(R.color.colorNotBlue));
                //age_edit_text.setTextColor(getResources().getColor(R.color.colorNotBlue));
                //name_edit_text.setHintTextColor(getResources().getColor(R.color.colorNotBlue));
                //age_edit_text.setHintTextColor(getResources().getColor(R.color.colorNotBlue));

            }
        });

        lightFemale.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onClick(View v) {
                if(mIsGendreClicked == true)
                {
                    mIsGendreClicked = false;
                    nextBtn.setEnabled(true);
                }
                gendre = "female";
                //HasGendre++;
                if (female.getVisibility() == View.INVISIBLE) {
                    female.setVisibility(View.VISIBLE);
                    male.setVisibility(View.INVISIBLE);
                } else if (female.getVisibility() == View.VISIBLE) {
                    //female.setVisibility(View.INVISIBLE);
                }
                //name_edit_text.setTextColor(getResources().getColor(R.color.colorPink));
                //age_edit_text.setTextColor(getResources().getColor(R.color.colorPink));
                //name_edit_text.setHintTextColor(getResources().getColor(R.color.colorPink));
                //age_edit_text.setHintTextColor(getResources().getColor(R.color.colorPink));
            }
        });

        nextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                global.setGendre(gendre);
                startActivity(new Intent(UserInfoSelectGendre.this, UserInfoPickNameAge.class));
            }
        });
    }
}
